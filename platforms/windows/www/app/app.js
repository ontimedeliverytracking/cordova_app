'use strict';


// Declare app level module which depends on filters, and services
var app = angular.module('app', [
        'ui.router',
        'ui.load',
        'ui.jq',
        'ngCookies',
        'ngStorage',
        'pascalprecht.translate',
        'ui.bootstrap',
        'app.directives',
        'app.controllers'
    ])
        .run(
        ['$rootScope', '$state', '$stateParams',
            function ($rootScope, $state, $stateParams) {
                $rootScope.$state = $state;
                $rootScope.$stateParams = $stateParams;
            }
        ]
    )
        .config(
        ['$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
            function ($stateProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide) {

                // lazy controller, directive and service
                app.controller = $controllerProvider.register;
                app.directive = $compileProvider.directive;
                app.filter = $filterProvider.register;
                app.factory = $provide.factory;
                app.service = $provide.service;
                app.constant = $provide.constant;

                $urlRouterProvider
                    //.otherwise('/access/signin');
                    .otherwise('/app/workerday');
                $stateProvider
                    // Access
                    .state('access', {
                        url: '/access',
                        template: '<div ui-view class="fade-in-right-big smooth"></div>'
                    })
                    .state('access.signin', {
                        url: '/signin',
                        templateUrl: 'app/access/view/signin.html'
                    })
                    .state('access.signup', {
                        url: '/signup',
                        templateUrl: 'app/access/view/signup.html'
                    })
                    .state('access.forgotpwd', {
                        url: '/forgotpwd',
                        templateUrl: 'app/access/view/forgotpwd.html'
                    })
                    .state('app', {
                        abstract: true,
                        url: '/app',
                        templateUrl: 'app/main/view/app.html'
                    })
                    .state('app.calendar', {
                        url: '/calendar',
                        templateUrl: 'app/calendar/view/app_calendar.html',
                        // use resolve to load other dependences
                        resolve: {
                            deps: ['uiLoad',
                                function (uiLoad) {
                                    return uiLoad.load(['css/fullcalendar.css',
                                        'js/jquery/fullcalendar/fullcalendar.min.js',
                                        'js/jquery/jquery-ui-1.10.3.custom.min.js',
                                        'js/jquery/fullcalendar/fullcalendar.js',
                                        'app/calendar/ui-calendar.js',
                                        'app/calendar/service/calendar.js',
                                        'app/calendar/controller/fullCalendarController.js',
                                    ]);
                                }]
                        }
                    })
                    .state('app.workerday', {
                        url: '/workerday',
                        templateUrl: 'app/workerday/view/app_workerday.html',
                        // use resolve to load other dependences
                        resolve: {
                            deps: ['uiLoad',
                                function (uiLoad) {
                                    return uiLoad.load(['css/fullcalendar.css',
                                        'js/jquery/fullcalendar/fullcalendar.min.js',
                                        'js/jquery/jquery-ui-1.10.3.custom.min.js',
                                        'js/jquery/fullcalendar/fullcalendar.js',
                                        'app/workerday/ui-calendar.js',
                                        'app/workerday/controller/workerdayController.js',
                                        'app/workerday/service/workerday.js',
                                    ]);
                                }]
                        }
                    })
            }
        ]
    )
        .config(['$translateProvider', function ($translateProvider) {

            // Register a loader for the static files
            // So, the module will search missing translation tables under the specified urls.
            // Those urls are [prefix][langKey][suffix].
            $translateProvider.useStaticFilesLoader({
                prefix: 'l10n/',
                suffix: '.json'
            });

            // Tell the module what language to use by default
            $translateProvider.preferredLanguage('en');

            // Tell the module to store the language in the local storage
            $translateProvider.useLocalStorage();

        }])
        .constant('JQ_CONFIG', {
            slider: ['js/jquery/slider/bootstrap-slider.js',
                'js/jquery/slider/slider.css']
        }
    )
    ;