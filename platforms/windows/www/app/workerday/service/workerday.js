app.service('ConnectAPI', function ($http) {
    var url = "http://54.186.217.234:8080";
    //var url = "http://localhost:3000/";
    this.getAllEvents = function () {
        return $http.get(url + '/api/events');
    },
        this.addEvents = function (events, start) {
            return $http.post(url + '/api/events', {
                events: events
            });
        },
        this.updateEvent = function (event, id) {
            return $http.put(url + '/api/events/' + id, event);
        },
        this.updateEvents = function (eventsUpdate) {
            return $http.put(url + '/api/events', {
                events: eventsUpdate
            });
        },
        this.deleteEventByDate = function (date) {
            return $http.post(url + '/api/events/delete', {
                date: date.toString()
            });
        }
})
app.service('Noti', function () {
    this.showNoti = function (type, text) {
        if (type == 1) {
            Lobibox.notify('info', {
                size: "mini",
                delay: 3000,
                position: 'bottom right',
                msg: text
            });
        }
        else if (type == 2) {
            Lobibox.notify('warning', {
                size: "mini",
                delay: 3000,
                position: 'bottom right',
                msg: text
            });
        }
        else if (type == 3) {
            Lobibox.notify('error', {
                size: "mini",
                delay: 3000,
                position: 'bottom right',
                msg: text
            });
        }
        else {
            Lobibox.notify('success', {
                size: "mini",
                delay: 3000,
                position: 'bottom right',
                msg: text
            });
        }
    }
});
