app.controller('WorkerdayController', ['$scope', '$modal', '$http', '$q', 'ConnectAPI', 'Noti', function ($scope, $modal, $http, $q, ConnectAPI, Noti) {

    $scope.mode = 'idle';
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $scope.events = [];
    $scope.eventSources = [$scope.events];

    /* event sources array*/

    ConnectAPI.getAllEvents().success(function (data) {
        $scope.events.splice(0);
        for (var i = 0; i < data.result.length; i++) {
            $scope.events.push(data.result[i]);
        }
    }).error(function (err) {
    });

    $scope.refresh = function (calendar) {
        ConnectAPI.getAllEvents().success(function (data) {
            $scope.events.splice(0);
            for (var i = 0; i < data.result.length; i++) {
                $scope.events.push(data.result[i]);
            }
            alert($scope.events.length);
        }).error(function (err) {

        });
    }

    /* alert on eventClick */
    $scope.alertOnEventClick = function (event, jsEvent, view) {
        $scope.showModalConfirmDeliveryEvent(event);
    };

    /* alert on Drop */
    alertOnDrop = function (event, delta, revertFunc, jsEvent, ui, view) {
        $scope.alertMessage = ('Event Droped to make dayDelta ' + delta);
    };

    /* alert on Resize */
    alertOnResize = function (event, delta, revertFunc, jsEvent, ui, view) {
        $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
    };

    $scope.overlay = $('.fc-overlay');
    alertOnMouseOver = function (event, jsEvent, view) {
        $scope.event = event;
        $scope.overlay.removeClass('left right').find('.arrow').removeClass('left right top pull-up');
        var wrap = $(jsEvent.target).closest('.fc-event');
        var cal = wrap.closest('.calendar');
        var left = wrap.offset().left - cal.offset().left;
        var right = cal.width() - (wrap.offset().left - cal.offset().left + wrap.width());
        if (right > $scope.overlay.width()) {
            $scope.overlay.addClass('left').find('.arrow').addClass('left pull-up')
        } else if (left > $scope.overlay.width()) {
            $scope.overlay.addClass('right').find('.arrow').addClass('right pull-up');
        } else {
            $scope.overlay.find('.arrow').addClass('top');
        }
        (wrap.find('.fc-overlay').length == 0) && wrap.append($scope.overlay);
    }

    /* config object */
    $scope.uiConfig = {
        calendar: {
            defaultView: "agendaDay",
            height: 450,
            fixedWeekCount: 2,
            allDaySlot: false,
            minTime: "00:00:00",
            maxTime: "23:00:00",
            slotEventOverlap: false,
            businessHours: {
                start: '10:00', // a start time (10am in this example)
                end: '18:00', // an end time (6pm in this example)
                dow: [1, 2, 3, 4]
                // days of week. an array of zero-based day of week integers (0=Sunday)
                // (Monday-Thursday in this example)
            },
            editable: true,
            businessHours: true,
            header: {
                left: 'prev',
                center: 'title',
                right: 'next'
            },
            eventClick: $scope.alertOnEventClick,
            eventDrop: alertOnDrop,
            eventResize: alertOnResize,
            // eventMouseover: alertOnMouseOver
        }
    };

    $scope.onCalendarResize = function (view) {
        debugger;
    }

    $scope.eventSelected = function (calEvent, jsEvent, view) {
        debugger;
    };

    /* remove event */
    $scope.remove = function (index) {
        $scope.events.splice(index, 1);
    };

    /* Change View */
    $scope.changeView = function (view, calendar, date) {
        if (view == 'agendaDay') {
            calendar.fullCalendar('changeView', view).fullCalendar('gotoDate', date);
        }
        else {
            calendar.fullCalendar('changeView', view);
        }
    };


    $scope.today = function (calendar) {
        calendar.fullCalendar('changeView', 'agendaDay');
        calendar.fullCalendar('today');
    };

    $scope.renderCalender = function (calendar) {
        if (calendar) {
            calendar.fullCalendar('render');
        }
    };

    $scope.getAllEventsInToday = function (date) {
        var events_today = [];
        for (var i = 0; i < $scope.events.length; i++) {
            var start = new Date($scope.events[i].start);
            if (start.getDate() === date.getDate() && start.getMonth() === date.getMonth()) {
                events_today.push($scope.events[i]);
            }
        }
        return events_today;
    }

    $scope.showModalSelectDelayEvent = function (event) {
        event.time_start = event.start.getDate() + "-" + event.start.getMonth() + "-" + event.start.getFullYear()
            + " " + event.start.getHours() + ":" + event.start.getMinutes() + ":" + event.start.getSeconds() + "0s";
        event.time_end = event.end.getDate() + "-" + event.end.getMonth() + "-" + event.end.getFullYear()
            + " " + event.end.getHours() + ":" + event.end.getMinutes() + ":" + event.end.getSeconds() + "0s";
        var modal = $modal.open({
            templateUrl: 'app/workerday/view/modalSelectDelayEvent.html',
            controller: selectDelayEventCtr,
            resolve: {
                event: function () {
                    return event;
                }
            }
        });
        //result when click ok button in modal
        modal.result.then(function (t) {
            $scope.delayTimeStartOfEvent(event, t);
        });
    };

    // Change time start when user talk will do event later.
    $scope.delayTimeStartOfEvent = function (event, time) {
        if (time == 0) {
            $scope.setDeliveryEvent(event, true);
        }
        else {
            var now = new Date();
            var events = $scope.getAllEventsInToday(now);
            for (var i = 0; i < events.length; i++) {
                if (events[i].start.getTime() >= event.start.getTime()) {
                    events[i].start = new Date(events[i].start.getTime() + time * 1000);
                    events[i].end = new Date(events[i].end.getTime() + time * 1000);
                }
            }
            $scope.deleteEventByDate(now);
            var eventsUpdate = [];
            for (var i = 0; i < events.length; i++) {
                $scope.events.push(events[i]);
                var event = {
                    id: events[i].id,
                    title: events[i].title,
                    type: events[i].type,
                    start: events[i].start.toString(),
                    end: events[i].end.toString(),
                    className: events[i].className,
                    location: events[i].location,
                    info: events[i].info,
                    lat: events[i].lat,
                    lon: events[i].lon,
                    delivery: events[i].delivery,
                    allDay: events[i].allDay,
                    new: events[i].new,
                    mail: events[i].mail,
                    time_before: events[i].time_before
                }
                eventsUpdate.push(event);
            }
            ConnectAPI.updateEvents(eventsUpdate).success(function (res) {
                Noti.showNoti(4, "Change time start of delivery successfully");
            }).error(function (err) {
                console.log(err);
            })
        }
    }

    /*show modal of add event*/
    var selectDelayEventCtr = function ($scope, $modalInstance, event) {
        $scope.event = event;

        $scope.ok = function (timeSelect) {
            $modalInstance.close(timeSelect);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };

    $scope.showModalConfirmDeliveryEvent = function (event) {
        var modal = $modal.open({
            templateUrl: 'app/workerday/view/modalConfirmDeliveryEvent.html',
            controller: selectDeliveryEventCtr
        });
        //result when click ok button in modal
        modal.result.then(function () {
            $scope.deliveryEvent(event);
        });
    };

    var selectDeliveryEventCtr = function ($scope, $modalInstance) {

        $scope.ok = function () {
            $modalInstance.close();
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };

    $scope.deliveryEvent = function (event) {
        $scope.setDeliveryEvent(event, true);
    }

    $scope.setDeliveryEvent = function (event, status) {
        for (var i = 0; i < $scope.events.length; i++) {
            if (event.start.getDate() == $scope.events[i].start.getDate()
                && event.start.getMonth() == $scope.events[i].start.getMonth()
                && event.start.getTime() == $scope.events[i].start.getTime()
                && event.end.getDate() == $scope.events[i].end.getDate()
                && event.end.getMonth() == $scope.events[i].end.getMonth()
                && event.end.getTime() == $scope.events[i].end.getTime()) {
                $scope.events[i].delivery = status;
                var eventAdd = {
                    id: $scope.events[i].id,
                    title: $scope.events[i].title,
                    type: $scope.events[i].type,
                    start: $scope.events[i].start,
                    end: $scope.events[i].end,
                    className: $scope.events[i].className,
                    location: $scope.events[i].location,
                    info: $scope.events[i].info,
                    lat: $scope.events[i].lat,
                    lon: $scope.events[i].lon,
                    delivery: $scope.events[i].delivery,
                    allDay: $scope.events[i].allDay,
                    new: $scope.events[i].new
                }
                ConnectAPI.updateEvent(eventAdd, eventAdd.id).success(function (res) {
                    Noti.showNoti(4, "Delivered");
                }).error(function (err) {
                    console.log(err);
                });
                break;
            }
        }
    }

    // use setTimeOut for check event ready start and noti to user
    var notiEventStart = function () {
       setTimeout(function () {
           var now = new Date();
           var events = $scope.getAllEventsInToday(now);
           for (var i = 0; i < events.length; i++) {
               if (events[i].start.getHours() == now.getHours() && events[i].start.getMinutes() == now.getMinutes()
                   && events[i].new == false && events[i].type == 'event' && events[i].delivery == false) {
                   $scope.showModalSelectDelayEvent(events[i]);
                   break;
               }
           }
           notiEventStart();
       }, 60000);
    }
    notiEventStart();


}
])
    ;