'use strict';

/* Controllers */

angular.module('app.controllers', [])
  .controller('AppCtrl', ['$scope', '$translate', '$localStorage', function($scope, $translate, $localStorage) {
	$scope.app = {
		  name: 'Angulr',
		  version: '1.0.3',
		  // for chart colors
		  color: {
			primary: '#7266ba',
			info:    '#23b7e5',
			success: '#27c24c',
			warning: '#fad733',
			danger:  '#f05050',
			light:   '#e8eff0',
			dark:    '#3a3f51',
			black:   '#1c2b36'
		  },
		  settings: {
			themeID: 1,
			navbarHeaderColor: 'bg-black',
			navbarCollapseColor: 'bg-white-only',
			asideColor: 'bg-black',
			headerFixed: true,
			asideFixed: false,
			asideFolded: false
		  }
		}

    // save settings to local storage
    if ( angular.isDefined($localStorage.settings) ) {
      $scope.app.settings = $localStorage.settings;
    } else {
      $localStorage.settings = $scope.app.settings;
    }
    $scope.$watch('app.settings', function(){ $localStorage.settings = $scope.app.settings; }, true);

    // angular translate
    $scope.langs = {en:'English', de_DE:'German', it_IT:'Italian'};
    $scope.selectLang = $scope.langs[$translate.proposedLanguage()] || "English";
    $scope.setLang = function(langKey) {
      // set the current lang
      $scope.selectLang = $scope.langs[langKey];
      // You can change the language during runtime
      $translate.use(langKey);
    };
  }])
  ;